import { Component, OnInit } from '@angular/core';
import {Observable, Subject, timer} from "rxjs";
import {PollingService} from "../services/polling.service";
import {repeatWhen, switchMap, takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-cache',
  templateUrl: './cache.component.html',
  styleUrls: ['./cache.component.css']
})
export class CacheComponent implements OnInit {

  private stopStream = new Subject();
  private startStream = new Subject();
  //@ts-ignore
  public  observeStream$: Observable<any>;

  constructor( private pollingService: PollingService) { }

  ngOnInit(): void {
    // @ts-ignore

    this.observeStream$ = timer(0, 500)
      .pipe(
        switchMap(() => this.pollingService.fetchUrl()),
        takeUntil(this.stopStream),
        // @ts-ignore
        repeatWhen(() =>this.startStream)
      );
  }

  start(){
    this.startStream.next();
    console.log('startsStream');
  }

  stop(){
    this.stopStream.next(event);
    console.log("stopsStream");
  }

}
