import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {LoginModule} from "./login/login.module";
import {BookModule} from "./books/book.module";
import {AuthorModule} from "./author/author.module";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import { RegisterComponent } from './register/register.component';
import { NavComponent } from './components/nav/nav.component';
import { OverviewComponent } from './overview/overview.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { BookListComponent } from './books/book-list/book-list.component';
import {AuthGuardService} from "./services/auth-guard.service";
import {AuthenticateService} from "./services/authenticate.service";
import {CreateAuthorListComponent} from "./author-list/create-author-list/create-author-list.component";
import {CreateAuthorComponent} from "./author/create-author/create-author.component";
import {AuthorServicesService} from "./services/author-services.service";
import { CacheComponent } from './cache/cache.component';
import { ConcatMapComponent } from './concat-map/concat-map.component';
import { MergeMapComponent } from './merge-map/merge-map.component';



@NgModule({
    declarations: [
        AppComponent,
        RegisterComponent,
        NavComponent,
        OverviewComponent,
        HomeComponent,
        PageNotFoundComponent,
      CreateAuthorListComponent,
      CacheComponent,
      ConcatMapComponent,
      MergeMapComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        LoginModule,
        BookModule,
        AuthorModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        HttpClientModule
    ],
    providers: [AuthGuardService, AuthenticateService, AuthorServicesService],
    exports: [
        RegisterComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
