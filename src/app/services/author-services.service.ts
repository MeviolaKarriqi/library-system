import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable, throwError, pipe} from "rxjs";
import {AuthorModel} from "../_models/author.model";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, map} from "rxjs/operators";
import {AuthorModule} from "../author/author.module";
@Injectable({
  providedIn: 'root'
})
export class AuthorServicesService {

  constructor(private fs: AngularFirestore,
              private http: HttpClient
              ) { }
  getAuthors() : Observable<AuthorModel []> {
    // @ts-ignore
    return this.fs.collection('authors').snapshotChanges()
      .pipe(map(snaps => {
        return snaps.map(snap => {
          return <AuthorModule>{
            id: snap.payload.doc.id,
            //@ts-ignore
            ...snap.payload.doc.data()
          };
        });
      }));
  }



   addAuthors(payload: AuthorModel){
     return this.fs.collection('authors').add(payload);
   }
   updateAuthors(authorId: string, payload: AuthorModel){
     return this.fs.doc('authors/'+authorId).update(payload);
   }
   deleteAuthors(authorId: string){
     return this.fs.doc('authors/'+authorId).delete();
   }
}


// getMockProducts(): Observable<ProductModel []>{
//   return this.db.collection('products').snapshotChanges()
//     .pipe(map(snaps =>{
//       return snaps.map( snap =>{
//         return<ProductModel>{
//           id: snap.payload.doc.id,
//           //@ts-ignore
//           ...snap.payload.doc.data()
//         };
//       });
//     }));
// }
