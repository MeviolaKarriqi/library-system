import { Injectable } from '@angular/core';
import {of} from "rxjs";
import {delay} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PollingService {

  constructor() { }
  //@ts-ignore
  random(min, max){
    const rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  }

  fetchUrl = () => of(`Generating http response ${Date.now()}`)
    .pipe(
      delay(this.random(100,300))
    );
}
