import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {UserModel} from "../_models/user.model";
import {HttpClient} from "@angular/common/http";
import {AuthorModel} from "../_models/author.model";

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private firestore: AngularFirestore,
              private httpClient: HttpClient) { }

  getAllUsers(): Observable<UserModel[]> {
    return this.firestore.collection('users').valueChanges() as Observable<UserModel[]>;
  }

  getUserById(id: number) {

  }

  getMockUsers(): Observable<UserModel[]> {
    return this.httpClient.get('assets/mocks/users.json') as Observable<UserModel[]>;
  }

}
