import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {Book} from "../books/create-book/book.model";

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private firestore: AngularFirestore) { }

  getAllBooks(): Observable<Book[]> {
    return this.firestore.collection('books').valueChanges() as Observable<Book[]>;
  }

}
