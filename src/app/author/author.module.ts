import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAuthorComponent } from './create-author/create-author.component';
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
    declarations: [
        CreateAuthorComponent
    ],
    exports: [
        CreateAuthorComponent
    ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class AuthorModule { }
