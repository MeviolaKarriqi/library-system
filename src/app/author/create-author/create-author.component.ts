import { AbstractControl } from '@angular/forms';
import { group } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import { CostumValidators } from 'src/app/shared/costum.validators';
import { FormControl } from '@angular/forms';
import {ActivatedRoute} from "@angular/router";
import {AuthorServicesService} from "../../services/author-services.service";
import {AuthorModel} from "../../_models/author.model";
import {BooksService} from "../../services/books.service";

@Component({
  selector: 'app-create-author',
  templateUrl: './create-author.component.html',
  styleUrls: ['./create-author.component.css']
})
export class CreateAuthorComponent implements OnInit {
  //@ts-ignore
  public author: AuthorModel;

  public createAuthorForm = this.fb.group({
    fullName: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
    contactPreference: ['email'],
    emailGroup: this.fb.group({
      email: ['', [Validators.required, CostumValidators.emailDomain]],
      confirmEmail: ['', Validators.required],
    }, { validator: this.matchEmails}),
    phone: [''],
    books: this.fb.array([
      this.addBooksFormGroup()
    ])
  });


  addBooksFormGroup(): FormGroup {
    return this.fb.group({
      bookName: ['', Validators.required],
      published: ['', Validators.required],
      genre: ['',Validators.required]
    });
  }


  get boosFormArray(): FormArray { return this.createAuthorForm.get('books') as FormArray }

  removeBookButtonClick(booksIndex: number ): void {
    const booksFromArray = <FormArray>this.createAuthorForm.get('books');
    booksFromArray.removeAt(booksIndex);
    booksFromArray.markAsDirty();
    booksFromArray.markAsTouched();
  }

  onContactPreferenceChange(selectedValue: string) {
    const phoneControl = this.createAuthorForm?.get('phone');
    const emailControl = this.createAuthorForm?.get('emailGroup')?.get('email');

    if (selectedValue === 'phone') {
      phoneControl?.setValidators(Validators.required);
      emailControl?.clearValidators();
    } else {
      phoneControl?.clearValidators();
      emailControl?.setValidators(Validators.required);
    }
    phoneControl?.updateValueAndValidity();
    emailControl?.updateValueAndValidity();
  }

  validationMessages = {
    'fullName': {
      'required': '*Full Name is required.',
      'minlength': '*Full Name must be greater than 2 characters.',
      'maxlength': '*Full Name must be less than 20 characters.'
    },
    'email': {
      'required': '*Email is required.',
      'emailDomain': '*Email domain should be gmail.com'
    },
    'confirmEmail': {
      'required': '*Confirm Email is required.',
    },
    'emailGroup': {
      'emailMismatch': '*Email and Confirm Email do not match'
    },
    'phone': {
      'required': '*Phone number is required.'
    },
    'bookName': {
      'required': '*Book Title is required.',
    },
    'published': {
      'required': '*Published Year is required.',
    },
    'genre': {
      'required': '*Genre is required.',
    },
  };

  formErrors = {
    'fullName': '',
    'email': '',
    'confirmEmail': '',
    'emailGroup': '',
    'phone': '',
    'bookName': '',
    'published': '',
    'genre': ''
  };

  onSubmit():void{
    this.mapFormValuesToAuthorsData();
    //@ts-ignore
    this.authorService.updateAuthors(this.authorr).subscribe(
      //@ts-ignore
      ()=> this.route.navigate (['/author-list']),
      (err: any) => console.log(err)
    )
  }

  mapFormValuesToAuthorsData(){
    this.author.fullName = this.createAuthorForm.value.fullName;
    this.author.emailGroup = this.createAuthorForm.value.emailGroup;
    this.author.contactPreference = this.createAuthorForm.value.contactPreference;
    this.author.email = this.createAuthorForm.value.email;
    this.author.phone = this.createAuthorForm.value.phone;
    this.author.books = this.createAuthorForm.value.books;
  }

  ngOnInit(){



    this.createAuthorForm.valueChanges.subscribe((data)=> {
      this.logValidationErrors(this.createAuthorForm);
    });

    this.route.paramMap.subscribe(params =>{
      //@ts-ignore
      const authId = params.get(id);
      if (authId) {
        //@ts-ignore
        this.getAuthorr(authId);
      }
    })

    this.createAuthorForm?.get('contactPreference')?.valueChanges.subscribe((data: string) => {
    this.onContactPreferenceChange(data);
    });
  }
  // @ts-ignore
  getAuthorr( id: string) {
    // @ts-ignore
    this.authorService.getAuthors(id).subscribe(
      //@ts-ignore
      (author: AuthorModel) => {this.editAuthor(author),
      this.author = author;},
      (err: any) => console.log(err)
    );
  }

  editAuthor(author: AuthorModel){
    this.createAuthorForm.patchValue({
      fullName: author.fullName,
      contactPreference: author.contactPreference,
      emailGroup: {
        email: author.email,
        confirmEmail: author.email
      },
      phone: author.phone,
      books: author.books
      })

    this.createAuthorForm
      //@ts-ignore
      .setControl('books', this.setExistingBooks(author.books));
  }
  // @ts-ignore
  setExistingBooks(booksSets: BooksService []): FormArray{
    const formArray = new FormArray([]);
    booksSets.forEach( b => {
      formArray.push(this.fb.group({
        //@ts-ignore
        bookName: b.bookName,
        //@ts-ignore
        published: b.published,
        //@ts-ignore
        genre: b.genre

      }));
    });
    return formArray;
  }





  logValidationErrors(group: FormGroup = this.createAuthorForm): void{
    Object.keys(group.controls).forEach((key: string)=>{

      const abstractControl = group.get(key);

      //@ts-ignore
      this.formErrors[key] = '';
      if(abstractControl && !abstractControl.valid
        && (abstractControl.touched || abstractControl.dirty || abstractControl.value !== '')){
        // @ts-ignore
        const messages = this.validationMessages[key];

        for(const errorKey in abstractControl.errors) {
          if(errorKey) {
            // @ts-ignore
            this.formErrors[key] += messages[errorKey] + ' ';
          }
        }
      }
      if(abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      }
      if(abstractControl instanceof FormArray) {
        for(const control of abstractControl.controls){
          if(control instanceof FormGroup){
            this.logValidationErrors(control);
          }
        }
      }
    });
  }


  addBookButtonClick(){
    return (<FormArray>this.createAuthorForm.get('books')).push(this.addBooksFormGroup());
  }


  onLoadDataClick(): void{

  }

  addAuthorData(){


  }

    constructor(private fb: FormBuilder,
                private route: ActivatedRoute,
                public authorService: AuthorServicesService) { }

  matchEmails(group: AbstractControl): { [key: string]: any } | null {
    const emailControl = group.get('email');
    const confirmEmailControl = group.get('confirmEmail');

    if (emailControl?.value === confirmEmailControl?.value || confirmEmailControl?.pristine) {
      return null;
    } else {
      return { 'emailMismatch': true };
    }
  }
}
