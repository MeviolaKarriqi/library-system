import { Component, OnInit } from '@angular/core';
import {ApiService} from "../services/api.service";
import {map, mergeAll, mergeMap} from "rxjs/operators";
import {flatMap} from "rxjs/internal/operators";
import {from} from "rxjs";
import {Student} from "../_models/student.model";


@Component({
  selector: 'app-merge-map',
  templateUrl: './merge-map.component.html',
  styleUrls: ['./merge-map.component.css']
})
export class MergeMapComponent implements OnInit {

  public students: Array<Student> = [];
  public totalStudents: number = 0;
  //@ts-ignore
  public student: Student;
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
  }

  getAllStudents() {
    this.apiService.get('http://localhost:8081/students').pipe(
      flatMap(resp => {
        return this.apiService.get(`http://localhost:8081/students/count`).pipe(
          map(countResp => {
            // resp['data']['totalRecords'] = resp['data']['totalRecords'];

            return {
              // @ts-ignore
              rows: resp['data'],
              // @ts-ignore
              totalRecords: countResp['data']['totalRecords']
            }
          })
        )
      })
    )
      .subscribe(resp => {
        console.log('in Subscription', resp)
        this.students       = resp['rows'];
        this.totalStudents  = resp['totalRecords'];
      });
  }

  getOneStudent(id:number = 1) {
    this.apiService.get(`http://localhost:8081/students/student/${id}`).pipe(
      map(resp => {
        // @ts-ignore
        return resp['data']
      }),
      mergeMap(param => {
        const studentId = param['id'];
        return this.apiService.get(`http://localhost:8081/students/subjects/${studentId}`).pipe(
          map(resp => {
            // @ts-ignore
            param['subjects'] = resp['data']['rows'];
            return param;
          })
        )
      })
    ).subscribe((resp) =>{
      console.log('Subscription called', resp)
      this.student = resp;
    })
  }

  // test map & mergeAll
  testMergeAll() {
    from([1,2,3,4]).pipe(
      // @ts-ignore
      mergeMap(id => {
        return this.apiService.get(`http://localhost:8081/students/student/${id}`)
      }),
      mergeAll()
    ).subscribe(val => console.log(val));
  }

}
