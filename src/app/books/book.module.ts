import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateBookComponent } from './create-book/create-book.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { BookComponent } from './book/book.component';
import {AvailabilityPipe} from "../_pipes/availability.pipe";
import {BookListComponent} from "./book-list/book-list.component";



@NgModule({
  declarations: [
    CreateBookComponent,
    BookListComponent,
    BookComponent,
    AvailabilityPipe

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CreateBookComponent,
    BookComponent
  ]
})
export class BookModule { }
