import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAuthorListComponent } from './create-author-list/create-author-list.component';
import {BrowserModule} from "@angular/platform-browser";
import {ReactiveFormsModule} from "@angular/forms";
import {AppModule} from "../app.module";


@NgModule({
  declarations: [
    CreateAuthorListComponent,
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        AppModule
    ],
  exports:[
    CreateAuthorListComponent
  ]
})
export class AuthorListModule { }
