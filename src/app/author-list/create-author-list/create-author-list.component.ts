import {Component, Input, OnInit} from '@angular/core';
import {AuthorModel} from "../../_models/author.model";
import {AuthorServicesService} from "../../services/author-services.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-author-list',
  templateUrl: './create-author-list.component.html',
  styleUrls: ['./create-author-list.component.css']
})
export class CreateAuthorListComponent implements OnInit {

  @Input() allAuthors: AuthorModel [] =[];
  constructor(private authorServices: AuthorServicesService,
              private router: Router) { }

  ngOnInit(): void {
    this.getAuthors();
  }
  getAuthors(){
    this.authorServices.getAuthors().subscribe( respond =>{ this.allAuthors = respond})
  }

  editButtonClick(authorID: string){
    this.router.navigate(['/edit', authorID]);
  }

}
