import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAuthorListComponent } from './create-author-list.component';

describe('CreateAuthorListComponent', () => {
  let component: CreateAuthorListComponent;
  let fixture: ComponentFixture<CreateAuthorListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAuthorListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAuthorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
