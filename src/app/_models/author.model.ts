import {BooksService} from "../services/books.service";


export interface AuthorModel{
  emailGroup: any;
  id: string;
  phone?: number;
  contactPreference: string;
  fullName: string;
  email: string;
  confirmEmail:string;
  books: BooksService;
}
