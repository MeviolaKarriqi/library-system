export interface Student {
  id: number;
  name: string;
  subjects?: any
}
