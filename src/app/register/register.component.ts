import {Component, Input, OnInit} from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Input() allAuthors: any[] =[];

  constructor( public fs : AngularFirestore) { }

  ngOnInit(): void {
    this.fs.collection('authors').valueChanges().subscribe(data => console.log(data));
  }

}
