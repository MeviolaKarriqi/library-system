import { Component, OnInit } from '@angular/core';
import {forkJoin} from "rxjs";
import {concatMap, tap} from "rxjs/operators";
import {ApiService} from "../services/api.service";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-concat-map',
  templateUrl: './concat-map.component.html',
  styleUrls: ['./concat-map.component.css']
})
export class ConcatMapComponent implements OnInit {

  newsList: any = [];
  constructor(private apiService: ApiService, private userService: UserService) { }
  ngOnInit(): void {
    // this.testConcatMap();
    this.getUserConfig();

    // this.testForkJoin();

  }
  testConcatMap() {
    this.apiService.get('http://localhost:8081/concatmap/userlist/5sec').pipe(
      tap(res => {console.log('First result', res);}),
      concatMap(res => this.apiService.get('http://localhost:8081/concatmap/products/3sec')),
      tap(res => {console.log('Second result', res)}),
      concatMap(res => this.apiService.get('http://localhost:8081/concatmap/news/1sec')),
      tap(res => console.log('Third result', res)),
    ).subscribe(resp => {
      console.log('final resp', resp)
    })

  }
  getUserConfig() {

    this.apiService.get(`http://localhost:8081/user/getUserInfo`).pipe(
      tap(res => {
        console.log('First result', res);
        // @ts-ignore
        this.userService.setUserInfo(res['userName'])
      }),
      // @ts-ignore
      concatMap(res => this.apiService.get(`http://localhost:8081/user/getUserTheme/${res['userId']}`)),
      tap(res => {
        console.log('Second result', res)
        // @ts-ignore
        this.userService.setUserTheme({'bodyBgColor': res['bodyBgColor'], 'bgColor': res['headerBgColor'], 'color': res['headerColor']})

      }),
      // @ts-ignore
      concatMap(res => this.apiService.get(`http://localhost:8081/user/getUserGeoLocation/${res['location']}`)),
      tap(res => console.log('Third result', res)),
    ).subscribe(resp => {
      console.log('final resp', resp)
    })
  }


  testForkJoin() {
    forkJoin([
      this.apiService.get('http://localhost:8081/user/getUserInfo').pipe(tap(res =>
      {
        console.log('first', res);
        // @ts-ignore
        this.userService.setUserInfo(res['userName'])
      })),
      this.apiService.get('http://localhost:8081/user/getCommonSetting').pipe(tap(res => console.log('second', res))),
      this.apiService.get('http://localhost:8081/news')
    ]).subscribe(
      allResults => {
        console.log('all result', allResults);
        this.newsList = allResults[2];
      }
    );
  }

}
