import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateBookComponent } from './books/create-book/create-book.component';
import { CreateAuthorComponent } from './author/create-author/create-author.component';
import {
    CreateAuthorListComponent
} from './author-list/create-author-list/create-author-list.component';
import { LoginComponent } from './login/login/login.component';
import {RegisterComponent} from "./register/register.component";
import {AuthGuardService} from "./services/auth-guard.service";
import {HomeComponent} from "./components/home/home.component";
import {BookListComponent} from "./books/book-list/book-list.component";
import {BookComponent} from "./books/book/book.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {CacheComponent} from "./cache/cache.component";
import {ConcatMapComponent} from "./concat-map/concat-map.component";
import {MergeMapComponent} from "./merge-map/merge-map.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent },  //, canActivate: [AuthGuardService]
  { path: 'books', component: BookListComponent },  //,canActivate: [AuthGuardService]
{ path: 'createBook', component: CreateBookComponent },   //, canActivate: [AuthGuardService]
  { path: 'login', component: LoginComponent },
  { path: 'book/:isbn', component: BookComponent},   //,  canActivate: [AuthGuardService]
  {path: 'author', component: CreateAuthorComponent},
  {path: 'edit/:id', component: CreateAuthorComponent},
  {path: 'author-list', component: CreateAuthorListComponent},
  {path: 'cache', component: CacheComponent},
  {path: 'concat-map', component: ConcatMapComponent},
  {path: 'merge-map', component: MergeMapComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
